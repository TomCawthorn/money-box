//=include calculator/**/*.js

(function($) {
    var form = $('#calculator-form');
    var response = $('.response');
    response.hide();

    form.submit(function(e) {
        e.preventDefault();
        var field = $('.field', form);
        var value = $('.field__input', field).val();
        field.find('.field__error').remove();
        response.hide().find('.list').remove();

        if (!validator.validate(value)) {
            var errorMessage = "Whoops! Looks like you've done something wrong. " + validator.getErrorMessage();
            field.append($('<div class="field__error">').html(errorMessage));
            return false;
        }

        var coins = calculator.calculateCoinsFor(parser.getNumericValueFor(value));
        list = '<ul class="list">';
        for (var i = 0; i < coins.length; i++) {
            if (coins[i].qty > 0) {
                list += '<li class="list__item">'+ coins[i].qty +' x '+ coins[i].label +'</li>';
            }
        }
        list += '</ul>';

        response.append(list).show();
    });
})(jQuery);
