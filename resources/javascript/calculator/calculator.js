var calculator = {
    coins: [
        {
            label: '&pound;2',
            value: 200,
            qty: 0
        },
        {
            label: '&pound;1',
            value: 100,
            qty: 0
        },
        {
            label: '50p',
            value: 50,
            qty: 0
        },
        {
            label: '20p',
            value: 20,
            qty: 0
        },
        {
            label: '10p',
            value: 10,
            qty: 0
        },
        {
            label: '5p',
            value: 5,
            qty: 0
        },
        {
            label: '2p',
            value: 2,
            qty: 0
        },
        {
            label: '1p',
            value: 1,
            qty: 0
        }
    ],
    calculateCoinsFor: function(runningValue) {
        var remainder;

        for (var i = 0; i < calculator.coins.length; i++) {
            if (runningValue >= calculator.coins[i].value) {
                remainder = runningValue % calculator.coins[i].value;
                calculator.coins[i].qty = (runningValue - remainder) / calculator.coins[i].value;
                runningValue = remainder;
            } else {
                calculator.coins[i].qty = 0;
            }
        }

        return calculator.coins;
    }
};

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = {
        calculator: calculator
    };
} else {
    window.calculator = calculator;
}