validator = {
    errorMessage: '',
    validate: function(value) {
        if (value.indexOf('£') !== -1 && value.indexOf('p') !== -1 && value.indexOf('.') === -1) {
            validator.setErrorMessage("You need to remove either the '£' or 'p' symbol");
            return false;
        }
        var reg = /^£?(\d*?)(\.?)(\d{1,})(p?)$/;
        if (!reg.test(value)) {
            validator.setErrorMessage("Make sure you've added an amount and have only used '£' and/or 'p' symbols");
            return false;
        }
        return true;
    },
    setErrorMessage: function(message) {
        validator.errorMessage = message;
    },
    getErrorMessage: function() {
        return validator.errorMessage;
    }
};

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = {
        validator: validator
    };
} else {
    window.validator = validator;
}