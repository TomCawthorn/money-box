parser = {
    getNumericValueFor: function(stringValue) {
        numericValue = parseFloat(parser.tidyString(stringValue));

        if (parser.detectsPounds(stringValue)) {
            return parser.standardiseToPennyValue(numericValue);
        }
        return numericValue;
    },
    tidyString: function(value) {
        return value.replace('£', '').replace('p','');
    },
    detectsPounds: function(value) {
        return value.indexOf('£') !== -1 || value.indexOf('.') !== -1;
    },
    standardiseToPennyValue: function(value) {
        return Math.round(value * 100);
    }
};

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = {
        parser: parser
    };
} else {
    window.parser = parser;
}