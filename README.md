# Money Box - Tom Cawthorn
A simple application that will work out the minimum number of Sterling coins needed to make up a given amount.

## Running the app
To run the app:

1. Download and navigate to the project
2. Open web/index.html in a web browser
3. Follow the onscreen instructions

## Running the test suite
The test suite uses two libraries - [Chai assertion library](http://chaijs.com/) and [Mocha testing library](https://mochajs.org/).

You'll need node & npm to install these packages. I completed the project using Node version 6.9.1 and npm version 3.10.9.

[How to install npm](https://www.npmjs.com/get-npm)

When you're ready to go, install the modules

```
npm install
```

and run the test suite

```
npm test
```

Happy testing!
