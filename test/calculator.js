var expect = require('chai').expect;
var calculator = require('../resources/javascript/calculator/calculator').calculator;

describe('Calculator', function() {
    it('calculates the fewest number of coins for 15p', function() {
        var output = calculator.calculateCoinsFor(15);
        expect(output).to.deep.have.same.members([
            {
                label: '&pound;2',
                value: 200,
                qty: 0
            },
            {
                label: '&pound;1',
                value: 100,
                qty: 0
            },
            {
                label: '50p',
                value: 50,
                qty: 0
            },
            {
                label: '20p',
                value: 20,
                qty: 0
            },
            {
                label: '10p',
                value: 10,
                qty: 1
            },
            {
                label: '5p',
                value: 5,
                qty: 1
            },
            {
                label: '2p',
                value: 2,
                qty: 0
            },
            {
                label: '1p',
                value: 1,
                qty: 0
            }
        ]);
    });

    it('calculates the fewest number of coins for 1489p', function() {
        var output = calculator.calculateCoinsFor(1489);
        expect(output).to.deep.have.same.members([
            {
                label: '&pound;2',
                value: 200,
                qty: 7
            },
            {
                label: '&pound;1',
                value: 100,
                qty: 0
            },
            {
                label: '50p',
                value: 50,
                qty: 1
            },
            {
                label: '20p',
                value: 20,
                qty: 1
            },
            {
                label: '10p',
                value: 10,
                qty: 1
            },
            {
                label: '5p',
                value: 5,
                qty: 1
            },
            {
                label: '2p',
                value: 2,
                qty: 2
            },
            {
                label: '1p',
                value: 1,
                qty: 0
            }
        ]);
    });

    it('calculates the fewest number of coins for 714p', function() {
        var output = calculator.calculateCoinsFor(714);
        expect(output).to.deep.have.same.members([
            {
                label: '&pound;2',
                value: 200,
                qty: 3
            },
            {
                label: '&pound;1',
                value: 100,
                qty: 1
            },
            {
                label: '50p',
                value: 50,
                qty: 0
            },
            {
                label: '20p',
                value: 20,
                qty: 0
            },
            {
                label: '10p',
                value: 10,
                qty: 1
            },
            {
                label: '5p',
                value: 5,
                qty: 0
            },
            {
                label: '2p',
                value: 2,
                qty: 2
            },
            {
                label: '1p',
                value: 1,
                qty: 0
            }
        ]);
    });
});