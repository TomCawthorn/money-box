var expect = require('chai').expect;
var validator = require('../resources/javascript/calculator/validator').validator;
var parser = require('../resources/javascript/calculator/parser').parser;
var calculator = require('../resources/javascript/calculator/calculator').calculator;

describe('Simple Integration Test', function() {
    it('calculates the minimum number of coins for sample input', function() {
        var input = '£13.98p';

        if (validator.validate(input)) {
            var coins = calculator.calculateCoinsFor(parser.getNumericValueFor(input));
            expect(coins).to.deep.have.same.members([
                {
                    label: '&pound;2',
                    value: 200,
                    qty: 6
                },
                {
                    label: '&pound;1',
                    value: 100,
                    qty: 1
                },
                {
                    label: '50p',
                    value: 50,
                    qty: 1
                },
                {
                    label: '20p',
                    value: 20,
                    qty: 2
                },
                {
                    label: '10p',
                    value: 10,
                    qty: 0
                },
                {
                    label: '5p',
                    value: 5,
                    qty: 1
                },
                {
                    label: '2p',
                    value: 2,
                    qty: 1
                },
                {
                    label: '1p',
                    value: 1,
                    qty: 1
                }
            ]);
        }
    });
});