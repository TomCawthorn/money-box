var expect = require('chai').expect;
var validator = require('../resources/javascript/calculator/validator').validator;

describe('Validator', function() {
    it('rejects a-z characters', function() {
        expect(validator.validate('abc')).to.equal(false);
    });

    it('rejects spaces', function() {
        expect(validator.validate(' ')).to.equal(false);
    });

    it('rejects an empty value', function() {
        expect(validator.validate('')).to.equal(false);
    });

    it('accepts 0-9 characters', function() {
        expect(validator.validate('123')).to.equal(true);
    });

    it('accepts a stirling symbol at beginning', function() {
        expect(validator.validate('£10.00')).to.equal(true);
    });

    it('rejects a stirling symbol at elsewhere', function() {
        expect(validator.validate('100£0')).to.equal(false);
    });

    it('accepts a pence symbol at the end', function() {
        expect(validator.validate('100p')).to.equal(true);
    });

    it('rejects a pence symbol elsewhere', function() {
        expect(validator.validate('1p00')).to.equal(false);
    });

    it('rejects a lonely pence symbol', function() {
        expect(validator.validate('p')).to.equal(false);
    });

    it('accepts a single period', function() {
        expect(validator.validate('12.34')).to.equal(true);
    });

    it('rejects multiple periods', function() {
        expect(validator.validate('10.10.00')).to.equal(false);
    });

    it ('accepts valid test cases', function() {
        expect(validator.validate('432')).to.equal(true);
        expect(validator.validate('213p')).to.equal(true);
        expect(validator.validate('213p')).to.equal(true);
        expect(validator.validate('£16.23p')).to.equal(true);
        expect(validator.validate('£14')).to.equal(true);
        expect(validator.validate('£54.04')).to.equal(true);
        expect(validator.validate('£23.33333')).to.equal(true);
        expect(validator.validate('001.41p')).to.equal(true);
    });

    it ('rejects invalid test cases', function() {
        expect(validator.validate('13x')).to.equal(false);
        expect(validator.validate('13p.02')).to.equal(false);
        expect(validator.validate('£p')).to.equal(false);
    });
});