var expect = require('chai').expect;
var parser = require('../resources/javascript/calculator/parser').parser;

describe('Parser', function() {
    // Parsing string for type of number entered.
    it('detects a pound value by decimal', function() {
        expect(parser.detectsPounds('10.50')).to.equal(true);
    });

    it('detects a pound value by stirling symbol', function() {
        expect(parser.detectsPounds('£10')).to.equal(true);
    });

    it('detects a pound value by stirling symbol and decimal', function() {
        expect(parser.detectsPounds('£10.50')).to.equal(true);
    });

    it('detects a pound value by stirling symbol, decimal and pence symbol', function() {
        expect(parser.detectsPounds('£10.50p')).to.equal(true);
    });

    it('detects penny value', function() {
        expect(parser.detectsPounds('1050')).to.equal(false);
    });

    it('detects penny value with pence symbol', function() {
        expect(parser.detectsPounds('1050p')).to.equal(false);
    });

    // Standardising values
    it('standardises a decimal value', function() {
        expect(parser.standardiseToPennyValue(10)).to.equal(1000);
    });

    // Processing given values
    it('returns type integer for given string', function() {
        expect(parser.getNumericValueFor('1000')).to.be.an('number');
    });

    it('returns integer value for penny input', function() {
        expect(parser.getNumericValueFor('432')).to.equal(432);
    });

    it('converts xx.xx to pennies', function() {
        expect(parser.getNumericValueFor('10.50')).to.equal(1050);
    });

    it('converts £xx.xx to pennies', function() {
        expect(parser.getNumericValueFor('£54.04')).to.equal(5404);
    });

    it('converts £xx.xxp to pennies', function() {
        expect(parser.getNumericValueFor('£16.23p')).to.equal(1623);
    });

    it('converts £xx to pennies', function() {
        expect(parser.getNumericValueFor('£14')).to.equal(1400);
    });

    it('converts xxxxp', function() {
        expect(parser.getNumericValueFor('213p')).to.equal(213);
    });

    it('converts £xx.xxxxxxx to xxxx', function() {
        expect(parser.getNumericValueFor('£23.33333')).to.equal(2333);
    });

    it('strips leading 0\'s from integer', function() {
        expect(parser.getNumericValueFor('0010')).to.equal(10);
    });

    it('strips leading 0\'s from float', function() {
        expect(parser.getNumericValueFor('001.41p')).to.equal(141);
    });
});