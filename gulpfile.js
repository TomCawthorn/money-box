var gulp         = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var include      = require('gulp-include');
var cleanCss    = require('gulp-clean-css');
var notify       = require('gulp-notify');
var sass         = require('gulp-sass');
var uglify       = require('gulp-uglify');
var livereload   = require('gulp-livereload');


gulp.task('sass', function() {
    return gulp.src('./resources/sass/*.scss')
        .pipe(include({
            includePaths: [
                __dirname + "/resources/javascript",
                __dirname + "/node_modules"
            ]
        }))
        .pipe(
            sass({
                sourceStyle: 'nested',
                includePaths: [
                    './node_modules',
                ]
            })
                .on('error', notify.onError())
        )
        .pipe(autoprefixer({browsers: ["last 4 versions", "Firefox >=25", "> 1%", "ie 9", "ie 8", "ie 7"],  cascade: true }))
        .pipe(cleanCss({compatibility: 'ie8'}))
        .pipe(gulp.dest('./web/css'))
        .pipe(notify("SASS compilation complete: <%=file.relative%>"))
        .pipe(livereload())
        .on('error', notify.onError())
        ;
});

gulp.task('js', function() {
    return gulp.src('./resources/javascript/*.js')
        .pipe(include({
            includePaths: [
                __dirname + "/resources/javascript",
                __dirname + "/node_modules"
            ]
        }))
        .pipe(uglify())
        .on('error', notify.onError())
        .pipe(gulp.dest('./web/javascript'))
        .pipe(notify("JS compilation complete: <%=file.relative%>"))
        .pipe(livereload())
        .on('error', notify.onError())
});

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('./resources/sass/**/*.scss', ['sass']);
    gulp.watch('./resources/javascript/**/*.js', ['js']);
});

gulp.task('default', [
    'sass',
    'js',
    'watch'
]);